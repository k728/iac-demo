---
- name: Install Docker
  hosts: all
  remote_user: adminuser
  become: true
  vars:
    gitlab_version: "latest"  # specify the GitLab version
    gitlab_home: "/path/to/gitlab/home"  # specify the path to the GitLab home directory

  tasks:
    - name: Install required system packages
      ansible.builtin.apt:
        name:
          - apt-transport-https
          - ca-certificates
          - curl
          - software-properties-common
          - python3-pip
          - virtualenv
          - python3-setuptools
        state: present
        update_cache: true

    - name: Create directory for Docker's GPG key
      ansible.builtin.file:
        path: /etc/apt/keyrings
        state: directory
        mode: '0755'

    - name: Add Docker GPG apt Key
      ansible.builtin.apt_key:
        url: https://download.docker.com/linux/ubuntu/gpg
        keyring: /etc/apt/keyrings/docker.gpg
        state: present

    - name: Add Docker Repository
      ansible.builtin.apt_repository:
        repo: deb https://download.docker.com/linux/ubuntu focal stable
        filename: docker
        state: present

    - name: Install Docker and related packages
      ansible.builtin.apt:
        name: "{{ item }}"
        state: present
        update_cache: true
      loop:
        - docker-ce
        - docker-ce-cli
        - containerd.io
        - docker-buildx-plugin
        - docker-compose-plugin

    - name: Enable and start Docker services
      ansible.builtin.systemd:
        name: "{{ item }}"
        enabled: true
        state: started
      loop:
        - docker.service
        - containerd.service

    - name: Deploy GitLab using Docker Compose
      community.docker.docker_compose:
        project_src: "/path/to/docker-compose-directory"  # directory containing docker-compose.yml
        state: present
        definition:
          version: '3.6'
          services:
            gitlab:
              image: "gitlab/gitlab-ce:{{ gitlab_version }}"
              container_name: gitlab
              restart: always
              hostname: 'gitlab.kingfamilyaz.com'
              environment:
                GITLAB_OMNIBUS_CONFIG: |
                  external_url 'https://gitlab.kingfamilyaz.com'
              ports:
                - '8080:80'
                - '9443:443'
                - '2222:22'
              volumes:
                - "{{ gitlab_home }}/config:/etc/gitlab"
                - "{{ gitlab_home }}/logs:/var/log/gitlab"
                - "{{ gitlab_home }}/data:/var/opt/gitlab"
              shm_size: '256m'

    - name: Retrieve GitLab initial root password
      ansible.builtin.command:
        cmd: docker exec gitlab grep 'Password:' /etc/gitlab/initial_root_password
      register: gitlab_root_password
      changed_when: false

    - name: Print GitLab initial root password
      ansible.builtin.debug:
        msg: "{{ gitlab_root_password.stdout }}"
